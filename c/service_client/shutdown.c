#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <xmmsclient/xmmsclient.h>
#include <xmmsclient/xmmsclient-glib.h>

/* This is a dummy callback handler. It just checks if there is any errors. */
void check_err (xmmsc_result_t *res, void *data)
{
	GMainLoop *ml = data;

	if (xmmsc_result_iserror (res)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (res));
		g_main_loop_quit (ml);
	}
}

/* This is just a dummy service method used to make the registration valid. */
void sc_dummy (xmmsc_connection_t *conn, xmmsc_result_t *res,
               xmmsc_service_method_t *method, void *data)
{
	if (xmmsc_result_iserror (res))
		fprintf (stderr, "%s\n", xmmsc_result_get_error (res));

	printf ("I'm a dummy service method\n");
}

/*
 * This is the callback function which will be called when shutdown broadcast is
 * received.
 */
void cb_shutdown (xmmsc_result_t *res, void *data)
{
	GMainLoop *ml = data;

	x_return_if_fail (ml);

	printf ("Shutting down...\n");

	/* Stop main loop. */
	g_main_loop_quit (ml);
}

int main ()
{
	/*
	 * As in xmms tutorial 6, we will use this main loop later for the
	 * service method.
	 */
	GMainLoop *ml;

	xmmsc_connection_t *connection;
	xmmsc_result_t *result;

	char service[] = "Dummy service";
	/* This is the method the service provides. */
	xmmsc_service_method_t *method;

	/*
	 * As usual, we create a connection first. Here we name it
	 * "sc_shutdown" since we are demonstrating shutdown broadcast.
	 */
	connection = xmmsc_init ("sc_shutdown");
	if (!connection) {
		fprintf (stderr, "OOM!\n");
		exit (1);
	}

	/* Establish the connection. */
	if (!xmmsc_connect (connection, getenv("XMMS_PATH"))) {
		fprintf (stderr, "Connection failed: %s\n",
		         xmmsc_get_last_error (connection));

		exit (1);
	}

	/*
	 * This registers a new service with the name "Dummy service" and the
	 * following string is the description of this service. The third argument 1
	 * is the major version of this service. And the last number 0 is the minor
	 * version.
	 */
	result = xmmsc_service_register (connection, service, "This is a sample"
	                                  " service which does nothing.", 1, 0);
	xmmsc_result_notifier_set (result, check_err, ml);
	xmmsc_result_unref (result);

	/*
	 * This is how we create the method. It has a name, a description, the
	 * callback function and user data to pass to the callback function.
	 */
	method = xmmsc_service_method_new ("Dummy method",
	                                   "This sample method does nothing.",
	                                   sc_dummy, NULL);

	/*
	 * This registers the service as well as the method to the server, and
	 * the callback function specified earlier will be attached to a
	 * broadcast by calling xmmsc_result_notifier_set() automatically, and
	 * pass the last argument to it as the user data, where in this case
	 * it's the connection pointer.
	 *
	 * We don't need to free the method, it will be freed automatically when
	 * it's no longer needed.
	 */
	result = xmmsc_service_method_register (connection, service, method);
	/* Remember to do the cleanup. */
	xmmsc_result_unref (result);

	/* Create the main loop. */
	ml = g_main_loop_new (NULL, FALSE);

	/* Here's the focus of this tutorial. Subscribe to the shutdown broadcast. */
	result = xmmsc_broadcast_service_shutdown (connection);
	/* Set up callback functions to be called when the broadcast arrives. */
	xmmsc_result_notifier_set (result, cb_shutdown, ml);
	/* Free the no longer needed result. */
	xmmsc_result_unref (result);

	/* Start the main loop and watch the magic. */
	xmmsc_mainloop_gmain_init (connection);
	g_main_loop_run (ml);

	xmmsc_unref (connection);

	return 0;
}
