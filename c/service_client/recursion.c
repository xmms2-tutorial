#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <xmmsclient/xmmsclient.h>
#include <xmmsclient/xmmsclient-glib.h>

/* This is a dummy callback handler. It just checks if there is any errors. */
void check_err (xmmsc_result_t *res, void *data)
{
	GMainLoop *ml = data;

	if (xmmsc_result_iserror (res)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (res));
		g_main_loop_quit (ml);
	}
}

/* This is the service method we will register to the server */
void sc_method (xmmsc_connection_t *conn, xmmsc_result_t *res,
                xmmsc_service_method_t *method, void *data)
{
	gchar *arg1;
	guint arg2;
	xmmsc_result_t *result;

	if (xmmsc_result_iserror (res)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (res));
		return;
	}

	/*
	 * These two arguments passed to this method is put into a dictionary,
	 * so we have to use xmmsc_result_get_dict_entry_# to get them out.
	 */
	if (!xmmsc_result_get_dict_entry_string (res, "arg_1", &arg1) ||
	    !xmmsc_result_get_dict_entry_uint (res, "arg_2", &arg2)) {
		xmmsc_service_method_error_set (method,
		                                "Failed to deserialize arguments.");
	} else {
		/* We don't want an eternal loop. */
		if (arg2 == 10)
			return;

		/* Print out the arguments. */
		printf ("argument 1 is: %s\n", arg1);
		printf ("argument 2 is: %d\n", arg2);

		/*
		 * Now we increase the number and call itself recursively.
		 */
		arg2++;
		xmmsc_service_method_arg_add_string (method, "arg_1", arg1);
		xmmsc_service_method_arg_add_uint32 (method, "arg_2", arg2);
		result = xmmsc_service_request (conn, "Service 1", method);
		xmmsc_result_notifier_set (result, check_err, data);

		/* Remember to do the cleanup. */
		xmmsc_result_unref (result);
	}
}

int main ()
{
	/*
	 * As in xmms tutorial 6, we will use this main loop later for the
	 * service method.
	 */
	GMainLoop *ml;

	xmmsc_connection_t *connection;
	xmmsc_result_t *result;

	char service[] = "Service 1";
	/* This is the method the service provides. */
	xmmsc_service_method_t *method;

	/*
	 * As usual, we create a connection first. Here we name it
	 * "sc_recursive" since we are registering a new recursive service method.
	 */
	connection = xmmsc_init ("sc_recursive");
	if (!connection) {
		fprintf (stderr, "OOM!\n");
		exit (1);
	}

	/* Establish the connection. */
	if (!xmmsc_connect (connection, getenv("XMMS_PATH"))) {
		fprintf (stderr, "Connection failed: %s\n",
		         xmmsc_get_last_error (connection));

		exit (1);
	}

	/* Create the main loop. */
	ml = g_main_loop_new (NULL, FALSE);

	/*
	 * This registers a new service with the name "Service 1" and the following
	 * string is the description of this service. The third argument 1 is the
	 * major version of this service. And the last number 0 is the minor
	 * version.
	 */
	result = xmmsc_service_register (connection, service, "This service"
	                                 " contains a method which calls itself"
	                                 " recursively.", 1, 0);
	xmmsc_result_notifier_set (result, check_err, ml);
	xmmsc_result_unref (result);

	/*
	 * This is how we create the method. It has a name, a description, the
	 * callback function and user data to pass to the callback function.
	 */
	method = xmmsc_service_method_new ("Method 1", "This method calls itself"
	                                   " recursively", sc_method, ml);

	/*
	 * Now we push the (name, type) pairs for each argument we want clients pass
	 * to the method.
	 */
	if (!xmmsc_service_method_arg_type_add (method, "arg_1",
	                                        XMMSC_SERVICE_ARG_TYPE_STRING, 0) ||
	    !xmmsc_service_method_arg_type_add (method, "arg_2",
	                                        XMMSC_SERVICE_ARG_TYPE_UINT32, 0)) {
		fprintf (stderr, "Unable to push arg types\n");
		exit (1);
	}

	/*
	 * This registers the service as well as the method to the server, and
	 * the callback function specified earlier will be attached to a
	 * broadcast by calling xmmsc_result_notifier_set() automatically, and
	 * pass the last argument to it as the user data, where in this case
	 * it's the connection pointer.
	 *
	 * We don't need to free the method, it will be freed automatically when
	 * it's no longer needed.
	 */
	result = xmmsc_service_method_register (connection, service, method);
	/* Remember to do the cleanup. */
	xmmsc_result_unref (result);

	/* Start the main loop and watch the magic. */
	xmmsc_mainloop_gmain_init (connection);
	g_main_loop_run (ml);

	xmmsc_unref (connection);

	return 0;
}
