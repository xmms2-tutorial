#include <stdio.h>
#include <stdlib.h>
#include <xmmsclient/xmmsclient.h>

int main ()
{
	/* As usual, we will use this later. */
	xmmsc_connection_t *connection;
	xmmsc_result_t *result;
	xmmsc_result_t *sresult;
	xmmsc_result_t *mresult;

	/*
	 * We will need these when we try to get information about the service
	 * and the method.
	 */
	char *sname;
	char *description;
	uint32_t major_version;
	uint32_t minor_version;
	uint32_t count;
	char *mname;
	xmmsc_service_method_t *method;

	/*
	 * As usual, we create a connection first. Here we name it
	 * "list" since we are listing all available services and methods.
	 */
	connection = xmmsc_init ("list");
	if (!connection) {
		fprintf (stderr, "OOM!\n");
		exit (1);
	}

	/* Establish the connection. */
	if (!xmmsc_connect (connection, getenv("XMMS_PATH"))) {
		fprintf (stderr, "Connection failed: %s\n",
		         xmmsc_get_last_error (connection));

		exit (1);
	}

	/* Get the list all available service IDs so that we can list them. */
	result = xmmsc_service_list (connection);
	xmmsc_result_wait (result);

	/* Check for errors. */
	if (xmmsc_result_iserror (result)) {
		fprintf (stderr, "%s\n",
		         xmmsc_result_get_error (result));
		return 1;
	}

	/*
	 * Now we use the IDs retrieved from the list to get details of each
	 * service.
	 */
	while (xmmsc_result_list_valid (result)) {
		if (!xmmsc_result_get_string (result, &sname)) {
			fprintf (stderr, "Unable to get service name.\n");
			return 1;
		}

		/* Now we can retrieve details of this service. */
		sresult = xmmsc_service_describe (connection, sname);
		xmmsc_result_wait (sresult);

		/* Check for errors. */
		if (xmmsc_result_iserror (sresult)) {
			fprintf (stderr, "%s\n",
					 xmmsc_result_get_error (sresult));
			return 1;
		}

		/* Parse the result. */
		xmmsc_result_get_dict_entry_string (sresult, "description",
		                                    &description);
		xmmsc_result_get_dict_entry_uint (sresult, "major_version",
		                                  &major_version);
		xmmsc_result_get_dict_entry_uint (sresult, "minor_version",
		                                  &minor_version);
		xmmsc_result_get_dict_entry_uint (sresult, "count", &count);

		/* Pretty print details. */
		printf ("%s\n"
		        "description: %s\n"
		        "major version: %d\n"
		        "minor version: %d\n"
		        "method count: %d\n",
				sname, description, major_version, minor_version, count);

		/*
		 * Since the variables are owned by the result, don't unref it unless
		 * you've finished using the variables.
		 */
		xmmsc_result_unref (sresult);

		/*
		 * Now we can get the list of all available method IDs of this
		 * service.
		 */
		sresult = xmmsc_service_method_list (connection, sname);
		xmmsc_result_wait (sresult);

		/* Check for errors. */
		if (xmmsc_result_iserror (result)) {
			fprintf (stderr, "%s\n",
					 xmmsc_result_get_error (result));
			return 1;
		}

		/*
		 * Do the same to all methods. We iterate through the list and pretty
		 * print details of each method.
		 */
		while (xmmsc_result_list_valid (sresult)) {
			if (!xmmsc_result_get_string (sresult, &mname)) {
				fprintf (stderr, "Unable to get method name.\n");
				return 1;
			}

			/* Now we can retrieve details of this method. */
			mresult = xmmsc_service_method_describe (connection, sname, mname);
			xmmsc_result_wait (mresult);

			/* Check for errors. */
			if (xmmsc_result_iserror (mresult)) {
				fprintf (stderr, "%s\n",
						 xmmsc_result_get_error (mresult));
				return 1;
			}

			/* Get the method structure. */
			xmmsc_result_get_service_method (mresult, &method);
			/*
			 * You need to reference the method before unreferencing the result,
			 * if you want to use the method later.
			 */
			xmmsc_service_method_ref (method);
			xmmsc_result_unref (mresult);

			/*
			 * This is the same as retrieving service attributes. All returned
			 * values are owned by the method.
			 */
			xmmsc_service_method_attribute_get (method, NULL, &description);

			/* Pretty print details. */
			printf ("\t%s\n"
					"\tdescription: %s\n",
					mname, description);

			/* Remember to free the method structure after using it. */
			xmmsc_service_method_unref (method);

			/* Advance to the next method in the list. */
			xmmsc_result_list_next (sresult);
		}

		printf ("------------------------------\n\n");

		/* Clean up. */
		xmmsc_result_unref (sresult);

		/* Advance to the next service in the list. */
		xmmsc_result_list_next (result);
	}

	/* Remember to unref the result after using it. */
	xmmsc_result_unref (result);
	xmmsc_unref (connection);

	return 0;
}
