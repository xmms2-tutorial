#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <xmmsclient/xmmsclient.h>
#include <xmmsclient/xmmsclient-glib.h>

/*
 * This is the callback function where the returned values from the service
 * method will be passed to.
 */
void callback (xmmsc_result_t *res, void *data)
{
	GMainLoop *ml = data;
	gchar *ret;

	if (xmmsc_result_iserror (res)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (res));
		g_main_loop_quit (ml);
		return;
	}

	/*
	 * All returned values will be put into a single dictionary, so use
	 * xmmsc_result_get_dict_entry_#() to get them. In this case, we know
	 * that the return value is an unsigned integer.
	 */
	if (!xmmsc_result_get_dict_entry_string (res, "ret", &ret)) {
		printf ("Failed to deserialize the return value.\n");
		return;
	}

	/* Print out the return value. */
	printf ("Return value: %s.\n", ret);

	/* That's it, kill the main loop. */
	g_main_loop_quit (ml);
}

int main ()
{
	/* As usual, we will use this later. */
	GMainLoop *ml;
	xmmsc_connection_t *connection;
	xmmsc_result_t *result;

	/* We will need this when we try to get information about the method. */
	xmmsc_service_method_t *method;

	/*
	 * As usual, we create a connection first. Here we name it
	 * "request" since we are requesting a service.
	 */
	connection = xmmsc_init ("request");
	if (!connection) {
		fprintf (stderr, "OOM!\n");
		exit (1);
	}

	/* Establish the connection. */
	if (!xmmsc_connect (connection, getenv("XMMS_PATH"))) {
		fprintf (stderr, "Connection failed: %s\n",
		         xmmsc_get_last_error (connection));

		exit (1);
	}

	/*
	 * This gets the details of a method "Method 1". This is necessary if you
	 * want to call the method.
	 */
	result = xmmsc_service_method_describe (connection, "Service 1", "Method 1");
	xmmsc_result_wait (result);

	if (xmmsc_result_iserror (result)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (result));
		return 1;
	}

	/*
	 * This gets the method. In order to be able to use it later, we have to
	 * reference it. So later you can use xmmsc_service_method_attribute_get()
	 * helper function to retrieve anything you need from it.
	 */
	xmmsc_result_get_service_method (result, &method);
	xmmsc_service_method_ref (method);
	xmmsc_result_unref (result);

	/*
	 * Now we set the arguments to the values we want to pass to the service
	 * method.
	 */
	xmmsc_service_method_arg_add_string (method, "arg_1", "hello world");
	xmmsc_service_method_arg_add_uint32 (method, "arg_2", 5);

	/* Set up main loop */
	ml = g_main_loop_new (NULL, FALSE);

	/*
	 * This will make the service method call, and registers this client to
	 * the broadcast. So when the service method returns, we'll get
	 * informed.
	 */
	result = xmmsc_service_request (connection, "Service 1", method);
	/*
	 * Set up the callback function so that we can do this to the returned
	 * values.
	 */
	xmmsc_result_notifier_set (result, callback, ml);

	/* Start the main loop and watch the magic. */
	xmmsc_mainloop_gmain_init (connection);
	g_main_loop_run (ml);

	/* Remember to do the cleanup. */
	xmmsc_result_unref (result);
	xmmsc_service_method_unref (method);
	xmmsc_unref (connection);

	return 0;
}
