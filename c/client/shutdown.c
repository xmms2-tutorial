#include <stdio.h>
#include <stdlib.h>
#include <xmmsclient/xmmsclient.h>

int main ()
{
	/* As usual, we will use this later. */
	xmmsc_connection_t *connection;
	xmmsc_result_t *result;

	/*
	 * As usual, we create a connection first. Here we name it
	 * "shutdown" since we are requesting a service client to shutdown.
	 */
	connection = xmmsc_init ("shutdown");
	if (!connection) {
		fprintf (stderr, "OOM!\n");
		exit (1);
	}

	/* Establish the connection. */
	if (!xmmsc_connect (connection, getenv("XMMS_PATH"))) {
		fprintf (stderr, "Connection failed: %s\n",
		         xmmsc_get_last_error (connection));

		exit (1);
	}

	/*
	 * This will send a shutdown request to the service client which provides
	 * service "Dummy service".
	 */
	result = xmmsc_service_shutdown (connection, "Dummy service");
	xmmsc_result_wait (result);

	if (xmmsc_result_iserror (result))
		fprintf (stderr, "%s\n", xmmsc_result_get_error (result));

	/* Remember to do the cleanup. */
	xmmsc_result_unref (result);
	xmmsc_unref (connection);

	return 0;
}
