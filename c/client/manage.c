#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <xmmsclient/xmmsclient.h>
#include <xmmsclient/xmmsclient-glib.h>

/*
 * This is the callback function where the returned values from the service
 * method will be passed to.
 */
void cb_sc_ids (xmmsc_result_t *res, void *data)
{
	GMainLoop *ml = data;
	gchar *ret = NULL;

	if (xmmsc_result_iserror (res)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (res));
		g_main_loop_quit (ml);
		return;
	}

	/*
	 * All returned values will be put into a single dictionary, so use
	 * xmmsc_result_get_dict_entry_#() to get them.
	 */
	printf ("Installed service clients:\n");
	while (xmmsc_result_dict_entry_list_valid (res, "ids")) {
		if (!xmmsc_result_get_dict_entry_string (res, "ids", &ret)) {
			printf ("Failed to deserialize the return value.\n");
			return;
		}

		/* Print out the return value. */
		printf ("\t%s\n", ret);

		xmmsc_result_dict_entry_list_next (res, "ids");
	}

	/* That's it, kill the main loop. */
	g_main_loop_quit (ml);
}

int main ()
{
	/* As usual, we will use this later. */
	GMainLoop *ml;
	xmmsc_connection_t *connection;
	xmmsc_result_t *result;

	/* We will need this when we try to get information about the method. */
	xmmsc_service_method_t *method;

	/*
	 * As usual, we create a connection first. Here we name it
	 * "manage" since we are requesting scm methods.
	 */
	connection = xmmsc_init ("manage");
	if (!connection) {
		fprintf (stderr, "OOM!\n");
		exit (1);
	}

	/* Establish the connection. */
	if (!xmmsc_connect (connection, getenv("XMMS_PATH"))) {
		fprintf (stderr, "Connection failed: %s\n",
		         xmmsc_get_last_error (connection));

		exit (1);
	}

	/*
	 * This gets the details of a method "sc_ids" of service
	 * "se.xmms.scm.query".
	 */
	result = xmmsc_service_method_describe (connection, "se.xmms.scm.query",
	                                        "sc_ids");
	xmmsc_result_wait (result);

	if (xmmsc_result_iserror (result)) {
		fprintf (stderr, "%s\n", xmmsc_result_get_error (result));
		return 1;
	}

	/*
	 * This gets the method. In order to be able to use it later, we have to
	 * reference it.
	 */
	xmmsc_result_get_service_method (result, &method);
	xmmsc_service_method_ref (method);
	xmmsc_result_unref (result);

	/* Set up main loop */
	ml = g_main_loop_new (NULL, FALSE);

	/*
	 * This will make the service method call, and registers this client to
	 * the broadcast. So when the service method returns, we'll get
	 * informed.
	 */
	result = xmmsc_service_request (connection, "se.xmms.scm.query", method);
	/*
	 * Set up the callback function so that we can do this to the returned
	 * values.
	 */
	xmmsc_result_notifier_set (result, cb_sc_ids, ml);

	/* Start the main loop and watch the magic. */
	xmmsc_mainloop_gmain_init (connection);
	g_main_loop_run (ml);

	/* Remember to do the cleanup. */
	xmmsc_result_unref (result);
	xmmsc_service_method_unref (method);
	xmmsc_unref (connection);

	return 0;
}
